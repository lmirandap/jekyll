---
layout: post
author: Luis Miranda
---
Dragon Ball Super (comúnmente abreviado como DBS) es la cuarta entrega del anime de la franquicia Dragon Ball, que va desde el 5 de julio de 2015 hasta el 25 de marzo de 2018.

Está ambientada entre los episodios 288 y 289 de Dragon Ball Z y es la primera serie de televisión de Dragon Ball que presenta un nuevo argumento en 18 años desde el último episodio de Dragon Ball GT en 1997.

Un manga de Dragon Ball Super se estaba produciendo junto con el anime. La serie es desarrollada por Toei, en un proceso similar al de las películas de Dragon Ball, Dragon Ball Z, Dragon Ball GT animes y Dragon Ball Z: Batalla de Dioses y Dragon Ball Z: Resurrección ‘F’. La trama de la serie se desarrolla después de la saga Kid Buu, en medio de la brecha de diez años hacia el 28º Torneo Mundial de Artes Marciales.
